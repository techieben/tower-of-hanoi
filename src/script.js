//define towers in the DOM

const tower1 = document.getElementById('tower1');
const tower2 = document.getElementById('tower2');
const tower3 = document.getElementById('tower3');


//define discs in the DOM & add size values, smaller is larger

const disc1 = document.getElementById('disc1');
disc1.value = 1;
const disc2 = document.getElementById('disc2');
disc2.value = 2;
const disc3 = document.getElementById('disc3');
disc3.value = 3;
const disc4 = document.getElementById('disc4');
disc4.value = 4;


//define move counter in the DOM

let moveCountDisplay = document.getElementById('moveCountDisplay');


//variables to track current game mode, source & destination towers, and move counts

let currentMode = 'grab'; //or 'place'
let currentSource = tower1;
let currentDestination = tower2;
let moveCount = 0;


//click handlers for each tower

tower1.onclick = function () {
    console.log("tower1 clicked");
    clickTower(tower1);
}

tower2.onclick = function () {
    console.log("tower2 clicked");
    clickTower(tower2);
}

tower3.onclick = function () {
    console.log("tower3 clicked");
    clickTower(tower3);
}


//main game logic

function clickTower(tower) {
    if (currentMode === 'grab') {
        currentSource = tower;
        currentMode = 'place';
    }
    else {
        currentDestination = tower;
        moveDisc();
        currentMode = 'grab';
    }
}


//move disk from one tower to another

function moveDisc() {
    // if (currentDestination.childElementCount === 0 || currentDestination.lastElementChild.getAttribute('data-value') < currentSource.lastElementChild.getAttribute('data-value')) {
        if ((currentSource.childElementCount !== 0) && (currentDestination.childElementCount === 0 || currentDestination.lastElementChild.value < currentSource.lastElementChild.value)) {
        currentDestination.appendChild(currentSource.lastElementChild);
        countMove();
        checkForWin();
    }
    else {
        alert("Illegal Move");
    }
}


//count the number of moves & update the display

function countMove() {
    moveCount++;
    moveCountDisplay.innerHTML = moveCount;

}


//count disks & alert if win 

function checkForWin() {
    if (tower2.childElementCount === 4 || tower3.childElementCount === 4) {
        if (moveCount === 15) {
            alert("You won in 15 moves! That's a perfect game!")
        } else if (moveCount < 25) {
            alert("You won in "+moveCount+" moves! Pretty Good!");
        } else { alert("You won in "+moveCount+" moves! I wouldn't brag about it.") }
    }
}